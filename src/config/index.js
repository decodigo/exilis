'use strict';

var currentEnvironment = process.env.NODE_ENV || 'development';
var knexConfig = require('./knexfile')[currentEnvironment];

module.exports = {
  db: knexConfig
}
