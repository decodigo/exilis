'use strict';

exports.up = function(knex, Promise) {
  
  var statusTable = knex.schema.createTableIfNotExists('status', function (table) {
      table.increments();
      table.string('name', 150).unique().notNullable();
      table.timestamps();
    });

  var visibilityTable = knex.schema.createTableIfNotExists('visibility', function (table) {
      table.increments();
      table.string('name', 150).unique().notNullable();
      table.timestamps();
    });

  var postsTable = knex.schema.createTableIfNotExists('posts', function (table) {
    table.increments();
    table.string('uuid', 36).notNullable();
    table.string('title', 150).notNullable();
    table.string('slug', 150).unique().notNullable();
    table.text('markdown', 16777215).notNullable();
    table.text('html', 16777215).notNullable();
    table.string('meta_title', 150).notNullable();
    table.string('meta_description', 200).notNullable();
    table.string('language', 5).notNullable();
    table.integer('status_id').unsigned().index().references('id').inTable('status');
    table.integer('visibility_id').unsigned().index().references('id').inTable('visibility');
    table.timestamps();
  });
  
  return Promise.all([
    postsTable,
    visibilityTable,
    statusTable
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('posts'),
    knex.schema.dropTable('status'),
    knex.schema.dropTable('visibility')
  ]);
};
