'use strict';

exports.up = function(knex, Promise) {
  return knex.schema.createTableIfNotExists('users', function(table) {
      table.increments();
      table.string('uuid', 36).notNullable();
      table.string('name');
      table.string('password', 150).notNullable();
      table.string('email', 254).unique().notNullable();
      table.timestamps();
      return table;
    })
  .then(Promise.resolve);
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users')
    .then(Promise.resolve);
};
